
## 0.0.6 [07-06-2023]

* update master to 2023.1

See merge request itentialopensource/pre-built-automations/mysql-adapter-test!6

---

## 0.0.5 [05-22-2023]

* Merging pre-release/2022.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/mysql-adapter-test!2

---

## 0.0.4 [05-17-2022]

* Bug fixes and performance improvements

See commit 5ff5cde

---

## 0.0.3 [05-17-2022]

* Bug fixes and performance improvements

See commit 4733104

---

## 0.0.2 [05-17-2022]

* Bug fixes and performance improvements

See commit c14a911

---

## 0.0.7 [04-21-2022]

* Bug fixes and performance improvements

See commit 3983a14

---

## 0.0.6 [10-15-2020]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.5 [08-03-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n
